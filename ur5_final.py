import sys
import rospy
import copy
import moveit_commander
import moveit_msgs.msg
import numpy as np
from geometry_msgs.msg import Pose
import geometry_msgs
import tf

robot = moveit_commander.RobotCommander()
group = moveit_commander.MoveGroupCommander("manipulator")

#get end-effector to point (x0,y0,z0)
def get_to_point(x,y,z):
    #define the position of the end-effector to go backwards from y-direction.
    pose_target = Pose()
    pose_target.position.x = x
    pose_target.position.y = y
    pose_target.position.z = z
    orientation = geometry_msgs.msg.Quaternion(*tf.transformations.quaternion_from_euler(0, 1.57, 0))
    pose_target.orientation = orientation
    group.set_pose_target(pose_target)
    #returns a boolean indicating whether the planning and execution was successful.
    success = group.go(wait=True)
    #ensures that there is no residual movement
    group.stop()
    #Clear targets after planning with poses.
    group.clear_pose_targets()

def cartisian_path(waypoints, wpose, dx, dy, dz, scale=1):
    wpose.position.x += scale * dx
    wpose.position.y += scale * dy  # First move downwards in (z)
    wpose.position.z += scale * dz
    waypoints.append(copy.deepcopy(wpose))
    return waypoints, wpose

#This function initializes the position of the robot arm to a point:(0.4,0.4,0.8).
def set_up(group):
    # initialize moveit_commander and a rospy node
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node('moveit_draw_L', anonymous=True)
    #Instantiate a MoveGroupCommander object. This is an interface to a planning group (group of joints).
    #group = moveit_commander.MoveGroupCommander("manipulator")
    #Set initial pose of the robot arm by assigning joint angles to avoid singularity.
    tau = 2 * np.pi
    joint_goal = group.get_current_joint_values()
    target = [0.20884494544629995, -0.6130569980857485, -0.9840406222605029, -1.5421582463286416, -1.7787449850165284, 1.5699689032396247]
    joint_goal[0] = target[0]
    joint_goal[1] = target[1]
    joint_goal[2] = target[2]
    joint_goal[3] = target[3]
    joint_goal[4] = target[4]
    joint_goal[5] = target[5]  # 1/6 of a turn
    #reset ee position to start point (0.4,0.2,0.8).
    group.go(joint_goal, wait=True)
    group.stop()
    get_to_point(0.4,0.2,0.9)
    joint_angles = group.get_current_joint_values()
    print("Start point Joint Angles:", joint_angles)

# Draw letter "L" from beginning to the end using plan_cartesian_path method.
def plan_cartesian_path_L(group):
    #create waypoints list
    waypoints = []
    wpose = group.get_current_pose().pose

    #define trajectories of the end-effector.
    waypoints, wpose = cartisian_path(waypoints, wpose, dx=0, dy=0, dz=-0.3, scale=1)
    waypoints, wpose = cartisian_path(waypoints, wpose, dx=0, dy=+0.2, dz=0, scale=1)

    # Make the Cartesian path to be interpolated at a resolution of 1 cm, and disable the jump threshold by setting it to 0.0,
    (plan, fraction) = group.compute_cartesian_path(waypoints, 0.01, 0.0) 
    display_trajectory = moveit_msgs.msg.DisplayTrajectory()
    display_trajectory.trajectory_start = robot.get_current_state()
    display_trajectory.trajectory.append(plan)
    group.execute(plan, wait=True)
    joint_angles = group.get_current_joint_values()
    print("End of L Joint Angles:", joint_angles)
    #return planning info.
    # return plan, fraction

def plan_cartesian_path_B(group):
    #create waypoints list
    waypoints = []

    #define trajectories of the end-effector.
    # scale=1
    wpose = group.get_current_pose().pose
    waypoints, wpose = cartisian_path(waypoints, wpose, dx=0, dy=0.1, dz=-0.1, scale=1)
    waypoints, wpose = cartisian_path(waypoints, wpose, dx=0, dy=-0.1, dz=-0.1, scale=1)
    waypoints, wpose = cartisian_path(waypoints, wpose, dx=0, dy=0.1, dz=-0.1, scale=1)
    waypoints, wpose = cartisian_path(waypoints, wpose, dx=0, dy=-0.1, dz=-0.1, scale=1)
    waypoints, wpose = cartisian_path(waypoints, wpose, dx=0, dy=0, dz=0.4, scale=1)
    (plan, fraction) = group.compute_cartesian_path(waypoints, 0.01, 0.0) 
    display_trajectory = moveit_msgs.msg.DisplayTrajectory()
    display_trajectory.trajectory_start = robot.get_current_state()
    display_trajectory.trajectory.append(plan)
    group.execute(plan, wait=True)
 

# Draw letter "L" from end to the beginning using go_to_pose_goal method.
def go_to_pose_goal_sequence_Y(x0,y0,z0):
    get_to_point(x0,y0+0.1,z0-0.1)
    get_to_point(x0,y0+0.2,z0)
    get_to_point(x0,y0+0.1,z0-0.1)
    get_to_point(x0,y0+0.1,z0-0.3)
    joint_angles = group.get_current_joint_values()
    print("End of Y Angles:", joint_angles)

set_up(group)
plan_cartesian_path_L(group)

get_to_point(0.4,0.2,0.9)
plan_cartesian_path_B(group)

get_to_point(0.4,0.2,0.8)
go_to_pose_goal_sequence_Y(0.4,0.2,0.8) 
